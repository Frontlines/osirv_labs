import numpy as np
import cv2
import matplotlib.pyplot as plt
import math

def save(name, image):
    cv2.imwrite("./problem2/{0}.jpg".format(name), image)

def show(title, img):
    cv2.imshow(title, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def medianFilter(image, radius):
    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):
            for c in range(0,3):
                kernel = image[(i-radius//2):(i+radius//2),(j-radius//2):(j-radius//2), c]
                kernel = kernel.sort()
                median = kernel[int(math.ceil(len(kernel)/2))]
                image[i,j,c] = median
            #for k in range(kernel.shape[0]):
            #    for l in range(kernel.shape[1]):
            #        output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

def addNormalNoise(image, mu, sigma):
    out = image.astype(np.float32)
    noise = np.random.normal(mu, sigma, image.shape)
    out = out + noise
    out[out > 255] = 255
    out[out < 0] = 0

    return out.astype(np.uint8)

def addSalt_n_PepperNoise(image, percent = 10):
    out = image.astype(np.float32)
    limit = ((float(percent)/2.0)/100.0) * 255.0
    noise = np.random.uniform(0,255, image.shape)
    out[noise < limit] = 0
    out[noise > (255-limit)] = 255
    out[out > 255] = 255
    out[out < 0] = 0
    return out.astype(np.uint8)

for imagename in ["airplane", "boats"]:
    slika = cv2.imread('../../images/{0}.bmp'.format(imagename), 0)
    
    for sig in [5,15,35]:
            noisyImg = addNormalNoise(slika, 0, sig)
            show("NormalNoise_sig{0}".format(sig), noisyImg)
            #save("NormalNoise_sigma{0}".format(sig), noisyImg)
            for radius in [1,3,5,7]:
                median = cv2.medianBlur(noisyImg, radius)
                show("NormalNoise_sig{0}_radius{1}".format(sig, radius), median)
            
            for kernel in [(3,3),(5,5),(7,7)]:
                for sigma in [1,3,5]:
                    blur = cv2.GaussianBlur(noisyImg, kernel, sigma)
                    show("NormalNoise_sig{0}_kx{1}_ky{2}_sig{3}".format(sig, kernel[0], kernel[1], sigma), blur)
    
    for percent in [1,10]:
            noisyImg = addSalt_n_PepperNoise(slika, percent)
            show("Salt_n_PepperNoise_percent".format(percent), noisyImg)
            #save("Salt_n_PepperNoise_percent{0}".format(percent), noisyImg)
            for radius in [1,3,5,7]:
                median = cv2.medianBlur(noisyImg, radius)
                show("Salt_n_PepperNoise_percent{0}_radius{1}".format(percent, radius), median)
            
            for kernel in [(3,3),(5,5),(7,7)]:
                for sigma in [1,3,5]:
                    blur = cv2.GaussianBlur(noisyImg, kernel, sigma)
                    show("Salt_n_PepperNoise_percent{0}_kx{1}_ky{2}_sig{3}".format(percent, kernel[0], kernel[1], sigma), blur)
        

        