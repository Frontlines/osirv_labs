import numpy as np
import cv2
import matplotlib.pyplot as plt

def save(name, image):
    cv2.imwrite("./problem1/{0}.jpg".format(name), image)

def show(title, img):
    cv2.imshow(title, img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def showhist(title, img):
    hist, bins = np.histogram(img.flatten(), bins=256, range=(0,255))
    plt.vlines(np.arange(len(hist)), 0, hist)
    plt.title(title)
    plt.show()

def addUnfiromNoise(image, low, high):
    out = image.astype(np.float32)
    noise = np.random.uniform(low,high,image.shape)
    out = out + noise
    out[out > 255] = 255
    out[out < 0] = 0

    return out.astype(np.uint8)

def addNormalNoise(image, mu, sigma):
    out = image.astype(np.float32)
    noise = np.random.normal(mu, sigma, image.shape)
    out = out + noise
    out[out > 255] = 255
    out[out < 0] = 0

    return out.astype(np.uint8)

def addSalt_n_PepperNoise(image, percent = 10):
    out = image.astype(np.float32)
    limit = ((float(percent)/2.0)/100.0) * 255.0
    noise = np.random.uniform(0,255, image.shape)
    out[noise < limit] = 0
    out[noise > (255-limit)] = 255
    out[out > 255] = 255
    out[out < 0] = 0
    return out.astype(np.uint8)

slika = cv2.imread('../../images/baboon.bmp', 0)

for sig in [1,5,10,20,40,60]:
    noisyImg = addNormalNoise(slika,0,sig)
    show("NormalNoise", noisyImg)
    showhist("NormalNoise", noisyImg)
    save("NormalNoise_sigma{0}".format(sig), noisyImg)

for uni in [(-20,20),(-40,40),(-60,60)]:
    noisyImg = addUnfiromNoise(slika, uni[0], uni[1])
    show("UniformNoise", noisyImg)
    showhist("UniformNoise", noisyImg)
    save("UniformNoise_low{0}_high{1}".format(uni[0],uni[1]), noisyImg)

for percent in [5,10,15,20]:
    noisyImg = addSalt_n_PepperNoise(slika, percent)
    show("Salt_n_PepperNoise", noisyImg)
    showhist("Salt_n_PepperNoise", noisyImg)
    save("Salt_n_PepperNoise_percent{0}".format(percent), noisyImg)
