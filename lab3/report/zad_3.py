import numpy as np
import cv2

def save(name, image):
    cv2.imwrite("./problem2/{0}.jpg".format(name), image)

def convolve(image, kernel, method = 0):
    if(len(image.shape) == 2):
        output = np.zeros((image.shape[0] - kernel.shape[0] + 1,
                       image.shape[1] - kernel.shape[1] + 1))
    else:
        output = np.zeros((image.shape[0] - kernel.shape[0] + 1,
                       image.shape[1] - kernel.shape[1] + 1,
                       image.shape[2]))
    kernel_rev = kernel[::-1,::-1]

    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    if(method == 0):
        output[output<0] = 0
    else:
        output[output<0] *= -1

    output = output.astype(np.uint8)
    return output

slika = cv2.imread('../../images/lenna.bmp', 1)
slikaGray = cv2.imread('../../images/lenna.bmp', 0)

identity = np.array([
    [0,0,0],
    [0,1,0],
    [0,0,0]])
save("Identity", convolve(slika, identity))

edgeDet1 = np.array([
    [1,0,-1],
    [0,0,0],
    [-1,0,1]])
save("EdgeDetection1", convolve(slikaGray, edgeDet1))

edgeDet2 = np.array([
    [0,1,0],
    [1,-4,1],
    [0,1,0]])
save("EdgeDetection2", convolve(slikaGray, edgeDet2))

edgeDet3 = np.array([
    [-1,-1,-1],
    [-1,8,-1],
    [-1,-1,-1]])
save("EdgeDetection3", convolve(slikaGray, edgeDet3))

sharpen = np.array([
    [0,-1,0],
    [-1,5,-1],
    [0,-1,0]])
save("Sharpen", convolve(slika, sharpen, 1))

boxblur = np.array([
    [1,1,1],
    [1,1,1],
    [1,1,1]]) * (1/9)
save("BoxBlur", convolve(slika, boxblur))

gaussblur1 = np.array([
    [1,2,1],
    [2,4,2],
    [1,2,1]]) * (1/16)
save("GaussianBlur3x3", convolve(slika, gaussblur1))

gaussblur2 = np.array([
    [1,4,6,4,1],
    [4,16,24,16,4],
    [6,24,36,24,6],
    [4,16,24,16,4],
    [1,4,6,4,1]]) * (1/256)
save("GaussianBlur5x5", convolve(slika, gaussblur2))

unsharpen = np.array([
    [1,4,6,4,1],
    [4,16,24,16,4],
    [6,24,-476,24,6],
    [4,16,24,16,4],
    [1,4,6,4,1]]) * (-1/256)
save("UnsharpMasking5x5", convolve(slika, unsharpen))

quit()