import numpy as np
import cv2

def save(name, image):
    cv2.imwrite("./problem6/{0}.bmp".format(name), image)

def quantiseWithNoise(image, q):
    image[image>255] = 255
    image[image<0] = 0

    noise = np.random.uniform(0,1,image.shape)

    d = 2**(8-q)
    
    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):
            image[i, j] = (np.floor((image[i, j]/d) + noise[i, j]) + (1/2)) * d

    image = image.astype(np.uint8)
    return image

slika = cv2.imread('../../images/BoatsColor.bmp', 0)

for q in range(1,9):
    save("boats_{0}n".format(q), quantiseWithNoise(slika.astype(np.float32),q))
