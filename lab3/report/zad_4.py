from os import listdir
from os.path import isfile, join
import numpy as np
import cv2

def save(name, image):
    cv2.imwrite("./problem3/{0}_invert.jpg".format(name), image)

onlyfiles = [f for f in listdir("../../images/") if isfile(join("../../images/", f))]

for file in onlyfiles:
    slikaGray = cv2.imread('../../images/{0}'.format(file), 0)
    save(file[:-4], np.invert(slikaGray))
