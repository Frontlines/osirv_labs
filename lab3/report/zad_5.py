from os import listdir
from os.path import isfile, join
import numpy as np
import cv2

def save(name, image, param):
    cv2.imwrite("./problem4/{0}_{1}_thresh.jpg".format(name, param), image)

onlyfiles = [f for f in listdir("../../images/") if isfile(join("../../images/", f))]

for file in onlyfiles:
    slika = cv2.imread('../../images/{0}'.format(file), 1)

    slika63 = slika.copy()
    slika63[slika63 <= 63] = 0
    slika127 = slika.copy()
    slika127[slika127 <= 127] = 0
    slika191 = slika.copy()
    slika191[slika191 <= 191] = 0
    save(file[:-4], slika63, 63)
    save(file[:-4], slika127, 127)
    save(file[:-4], slika191, 191)
