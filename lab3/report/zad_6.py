import numpy as np
import cv2

def save(name, image):
    cv2.imwrite("./problem5/{0}.bmp".format(name), image)

def quantise(image, q):
    image[image>255] = 255
    image[image<0] = 0

    d = 2**(8-q)
    
    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):
            image[i, j] = (np.floor((image[i, j]/d)) + (1/2)) * d

    return image.astype(np.uint8)

slika = cv2.imread('../../images/BoatsColor.bmp', 0)

for q in range(1,9):
    save("boats_{0}".format(q), quantise(slika.astype(np.float32),q))