import numpy as np
import cv2

def show(name, image):
    cv2.imshow(name, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def CreateBorderAroundImage(imgData, thickness):

    sourceShape = imgData.shape
    dimensions = (sourceShape[0]+thickness*2,sourceShape[1]+thickness*2,sourceShape[2])
    print(dimensions)
    borderedImg = np.zeros(dimensions, dtype=np.uint8)

    borderedImg[thickness:-thickness,thickness:-thickness ,:] = imgData[:,:,:]
    
    cv2.imwrite("./problem1/problem1_2_{0}.jpg".format(thickness), borderedImg)
    show("ImageWithBorder", borderedImg)

slika = cv2.imread('..//../images/baboon.bmp', 1)
show("original", slika)
CreateBorderAroundImage(slika, 20)
CreateBorderAroundImage(slika, 10)