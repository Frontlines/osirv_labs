import numpy as np
import cv2

def CropToSourceImage(sImg, oImg):
    sourceShape = sImg.shape

    newImg = np.zeros(sourceShape, dtype=np.uint8)

    newImg[:,:,:] = oImg[:sourceShape[0],:sourceShape[1],:sourceShape[2]]

    return newImg

slika1 = cv2.imread('../../images/baboon.bmp', 1)
slika2 = cv2.imread('../../images/airplane.bmp', 1)
slika3 = cv2.imread('../../images/lenna.bmp', 1)

stack1 = np.hstack((slika1,CropToSourceImage(slika1,slika2)))
final = np.hstack((stack1,CropToSourceImage(slika1,slika3)))

cv2.imshow("ImgRez", final)
cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.imwrite("./problem1/problem1_1.jpg", final)