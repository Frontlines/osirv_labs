import numpy as np
import cv2

def ExportImageToEachChannelBGR(imgData, color):
    imgCopy = imgData.copy()
    
    channelsToRemove = list(range(0 ,3, 1 + (color-1)%2))
    
    if((color-1) in channelsToRemove):
        channelsToRemove.remove(color-1)
    
    for channel in channelsToRemove:
        imgCopy[:,:, channel] = 0
        
    cv2.imshow("Img" + str(color), imgCopy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

slika = cv2.imread('../images/baboon.bmp', 1)

for i in range(1, 4):
    ExportImageToEachChannelBGR(slika, i)
