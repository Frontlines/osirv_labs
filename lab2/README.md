# Rezultati prvog/drugog zadatka

## Slika 1 (Plavi kanal)
![plavi](Img1.jpg "Plavi Kanal")

## Slika 2 (Zeleni kanal)
![zeleni](Img2.jpg "Zeleni Kanal")

## Slika 3 (Crveni kanal)
![crveni](Img3.jpg "Crveni Kanal")

# Rezultati treceg zadatka

## Slika sa obrubom
![obrub](BorderedImage.jpg "Slika sa obrubom")

# Rezultati cetvrtog zadatka

## Slika 1 (pola redova)
![pola_redova](slika1_polaRedova.jpg "Plavi Kanal")

## Slika 2 (pola stupaca)
![pola_stupaca](slika2_polaStupaca.jpg "Plavi Kanal")

## Slika 3 (pola i jednog i drugog)
![pola_svega](slika3_polaSvega.jpg "Plavi Kanal")
