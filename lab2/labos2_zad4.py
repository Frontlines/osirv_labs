import numpy as np
import cv2

slika = cv2.imread('../images/baboon.bmp', 1)

firstImg = slika.copy()
firstImg = firstImg[::2,:,:]
cv2.imwrite("slika1_polaRedova.jpg", firstImg)

secondImg = slika.copy()
secondImg = secondImg[:,::2,:]
cv2.imwrite("slika2_polaStupaca.jpg", secondImg)

thirdImg = slika.copy()
thirdImg = thirdImg[::2,::2,:]
cv2.imwrite("slika3_polaSvega.jpg", thirdImg)
