# Example problem report
## Author: Hrvoje Leventić

The program subsamples the images with factors `2, 4, 8`. Below are examples of 
subsampling by each factor. The function `subsample()` performs the subsampling of 
the image passed as first parameter by the factor passed as second parameter.

The `subsample` function returns the subsampled numpy array cast to `uint8`.
Subsampling is performed by taking every second, fourth, or eight row and
column of the input image, depending on the parameter `factor`

### Subsample by factor 2

![](airplane_sub2.png)

![](asbest_sub2.png)

![](baboon_sub2.png)

![](barbara_sub2.png)

![](boats_sub2.png)

![](BoatsColor_sub2.png)

![](goldhill_sub2.png)

![](lenna_sub2.png)

![](pepper_sub2.png)



### Subsample by factor 4

![](airplane_sub4.png)

![](asbest_sub4.png)

![](baboon_sub4.png)

![](barbara_sub4.png)

![](boats_sub4.png)

![](BoatsColor_sub4.png)

![](goldhill_sub4.png)

![](lenna_sub4.png)

![](pepper_sub4.png)

### Subsample by factor 8

![](airplane_sub8.png)

![](asbest_sub8.png)

![](baboon_sub8.png)

![](barbara_sub8.png)

![](boats_sub8.png)

![](BoatsColor_sub8.png)

![](goldhill_sub8.png)

![](lenna_sub8.png)

![](pepper_sub8.png)

