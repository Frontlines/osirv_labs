import cv2
import numpy as np
import os

PROJECT_PATH = "C:\\Users\\JURAJ.JURAJ-PC\\source\\repos\\osirv_labs"

def load_images(images_path, grayscale=1):
    """ Outputs: list of loaded images as numpy arrays and list of image filenames"""
    images = []
    image_names = []
    # os.listdir returns a list of filenames in a directory passed as parameter
    for image in os.listdir(images_path):
        image_path = os.path.join(images_path, image)
        if grayscale:
            open_as_flag = cv2.IMREAD_GRAYSCALE
        else:
            open_as_flag = cv2.IMREAD_COLOR
        print(image)
        images.append(cv2.imread(image_path, open_as_flag))
        image_names.append(image)
    return images, image_names

def show(image):
    # Break if trying to show image that is not uint8 datatype. You should always
    # convert your images to np.uint8 before trying to display them
    assert image.dtype == np.uint8, "Image is not np.uint8 datatype, convert to appropriate datatype"

    cv2.imshow("Image", image)
    cv2.waitKey()
    cv2.destroyAllWindows()

def save_image(image, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    print ("Base name for " + image_name + " is " + base_name)

    # Create absolute path to the project
    ######### Change this for your problem, e.g. os.path.join(PROJECT_PATH, "hw1", "problem_1")
    base_path = os.path.join(PROJECT_PATH, "hw1", "report", "problem4")
    save_path = os.path.join(base_path, base_name + append_string + ".png")
    print ("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

def checker_invert_image(image,thickness):
	checkered = image.copy()
	checkered = checkered.astype(np.int16)

	vertical_len = (checkered.shape[1] // thickness) + 1
	horizontal_len = (checkered.shape[0] // thickness) + 1

	if(len(checkered.shape) == 3):
		for vlength in range(0, vertical_len, 2):
			for hlength in range(0, horizontal_len, 2):
				check_dis = checkered[(hlength+1)*thickness:(hlength+2)*thickness,vlength*thickness:(vlength+1)*thickness,:]
				check = checkered[hlength*thickness:(hlength+1)*thickness,(vlength+1)*thickness:(vlength+2)*thickness,:]

				check_dis -= 255
				checkered[(hlength+1)*thickness:(hlength+2)*thickness,vlength*thickness:(vlength+1)*thickness,:] = np.absolute(check_dis)
				check -= 255
				checkered[hlength*thickness:(hlength+1)*thickness,(vlength+1)*thickness:(vlength+2)*thickness,:] = np.absolute(check)

	else:
		for vlength in range(0, vertical_len, 2):
			for hlength in range(0, horizontal_len, 2):
				check_dis = checkered[(hlength+1)*thickness:(hlength+2)*thickness,vlength*thickness:(vlength+1)*thickness]
				check = checkered[hlength*thickness:(hlength+1)*thickness,(vlength+1)*thickness:(vlength+2)*thickness]

				check_dis -= 255
				checkered[(hlength+1)*thickness:(hlength+2)*thickness,vlength*thickness:(vlength+1)*thickness] = np.absolute(check_dis)
				check -= 255
				checkered[hlength*thickness:(hlength+1)*thickness,(vlength+1)*thickness:(vlength+2)*thickness] = np.absolute(check)

	return checkered.astype(np.uint8)

images_path = os.path.join(PROJECT_PATH, "images")
images, image_names = load_images(images_path, grayscale=0)
img_dict = dict(zip(image_names, images))

for image in list(img_dict):
    for thickness in [8, 16, 32]:
        checkered = checker_invert_image(img_dict[image], thickness)

        show(checkered)
        save_image(checkered, image, "_CI{0}".format(thickness))