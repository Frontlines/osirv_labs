import cv2
import numpy as np
import os

PROJECT_PATH = "C:\\Users\\JURAJ.JURAJ-PC\\source\\repos\\osirv_labs"

def load_images(images_path, grayscale=1):
    """ Outputs: list of loaded images as numpy arrays and list of image filenames"""
    images = []
    image_names = []
    # os.listdir returns a list of filenames in a directory passed as parameter
    for image in os.listdir(images_path):
        image_path = os.path.join(images_path, image)
        if grayscale:
            open_as_flag = cv2.IMREAD_GRAYSCALE
        else:
            open_as_flag = cv2.IMREAD_COLOR
        print(image)
        images.append(cv2.imread(image_path, open_as_flag))
        image_names.append(image)
    return images, image_names

def show(image):
    # Break if trying to show image that is not uint8 datatype. You should always
    # convert your images to np.uint8 before trying to display them
    assert image.dtype == np.uint8, "Image is not np.uint8 datatype, convert to appropriate datatype"

    cv2.imshow("Image", image)
    cv2.waitKey()
    cv2.destroyAllWindows()

def save_image(image, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    print ("Base name for " + image_name + " is " + base_name)

    # Create absolute path to the project
    ######### Change this for your problem, e.g. os.path.join(PROJECT_PATH, "hw1", "problem_1")
    base_path = os.path.join(PROJECT_PATH, "hw1", "report", "problem5")
    save_path = os.path.join(base_path, base_name + append_string + ".png")
    print ("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

def border_image(imgData, thickness, dtype):
    sourceShape = imgData.shape

    if(len(sourceShape) == 3):
        dimensions = (sourceShape[0]+thickness*2,sourceShape[1]+thickness*2, sourceShape[2])
        borderedImg = np.zeros(dimensions, dtype=dtype)
        borderedImg[thickness:-thickness,thickness:-thickness ,:] = imgData[:,:,:]
    else:
        dimensions = (sourceShape[0]+thickness*2,sourceShape[1]+thickness*2)
        borderedImg = np.zeros(dimensions, dtype=dtype)
        borderedImg[thickness:-thickness,thickness:-thickness] = imgData[:,:]

    return borderedImg

def quantise_pixel(value, q):
    d = 2**(8-q)
    return (np.floor((value/d)) + (1/2)) * d

images_path = os.path.join(PROJECT_PATH, "images")
images, image_names = load_images(images_path, grayscale=0)
img_dict = dict(zip(image_names, images))

for image in list(img_dict):
    original = img_dict[image]

    for q in range(1,4):
        original_exp = border_image(original, 1, np.float32)

        if(len(original_exp.shape) == 3):
            for i in range(1, original.shape[1] + 1):
                for j in range(1, original.shape[0] + 1):
                    pixel = original_exp[j - 1, i - 1, :]
                    e = quantise_pixel(pixel, q) - pixel

                    original_exp[j, i + 1, :] = original_exp[j, i + 1, :] + (7/16) * e
                    original_exp[j + 1, i - 1, :] = original_exp[j + 1, i - 1, :] + (3/16) * e
                    original_exp[j + 1, i, :] = original_exp[j + 1, i, :] + (5/16) * e
                    original_exp[j + 1, i + 1, :] = original_exp[j + 1, i + 1, :] + (1/16) * e

            original_exp[original_exp > 255] = 255
            original_exp[original_exp < 0] = 0
            fsd = original_exp[1:-1, 1:-1, :].astype(np.uint8)
        else:
            for i in range(1, original.shape[1] + 1):
                for j in range(1, original.shape[0] + 1):
                    pixel = original_exp[j - 1, i - 1]
                    e = quantise_pixel(pixel, q) - pixel

                    original_exp[j, i + 1] = original_exp[j, i + 1] + (7/16) * e
                    original_exp[j + 1, i - 1] = original_exp[j + 1, i - 1] + (3/16) * e
                    original_exp[j + 1, i] = original_exp[j + 1, i] + (5/16) * e
                    original_exp[j + 1, i + 1] = original_exp[j + 1, i + 1] + (1/16) * e

            original_exp[original_exp > 255] = 255
            original_exp[original_exp < 0] = 0
            fsd = original_exp[1:-1, 1:-1].astype(np.uint8)

        show(fsd)
        save_image(fsd, image, "_{0}fsd".format(q))