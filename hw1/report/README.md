# Zadaća 1

## Problem 1
Zadatak je bio dodati horizontalne, odnosno vertikalne linije (širine 1 piksela) svim slikama unutar images direktorija ovog repozitorija.
Ovdje možete vidjet rezultat na jednoj od slika iz direktorija. Ovim postupkom smo postigli efekt, ja bi reko, CRT monitora.

![Horizontalno](./problem1/airplane_H.png "BoatsColor s horizontalnim linijama")
![Vertikalno](./problem1/airplane_V.png "BoatsColor s vertikalnim linijama")


## Problem 2
Zadatak je bio dodati horizontalne, odnosno vertikalne linije zadanih širina: 8, 16, 32 - svim slikama unutar images direktorija ovog repozitorija.
Ovdje možete vidjet rezultat na jednoj od slika iz direktorija. Ovim postupkom smo postigli efekt rešetki.

### Širina 8
![H8](./problem2/asbest_H8.png "Asbest s horizontalnim linijama širine 8")
![V8](./problem2/asbest_V8.png "Asbest s vertikalnim linijama širine 8")

### Širina 16
![H16](./problem2/asbest_H16.png "Asbest s horizontalnim linijama širine 16")
![V16](./problem2/asbest_V16.png "Asbest s vertikalnim linijama širine 16")

### Širina 32
![H32](./problem2/asbest_H32.png "Asbest s horizontalnim linijama širine 32")
![V32](./problem2/asbest_V32.png "Asbest s vertikalnim linijama širine 32")

# Problem 3
Zadatak je bio dodati kvadratiće uniformnih dimenzija svim slikama unutar images direktorija ovog repozitorija.
Ovdje možete vidjet rezultat na jednoj od slika iz direktorija. Ovim postupkom smo postigli efekt šahovnice.

### Dimenzije 8x8
![C8](./problem3/pepper_C8.png "Pepper s kvadratićima dimenzija 8x8")

### Dimenzije 16x16
![C16](./problem3/pepper_C16.png "Pepper s kvadratićima dimenzija 16x16")

### Dimenzije 32x32
![C32](./problem3/pepper_C32.png "Pepper s kvadratićima dimenzija 32x32")

# Problem 4
Zadatak je bio invertirati vrijednosti pixela unutar kvadratića uniformnih dimenzija na svim slikama unutar images direktorija ovog repozitorija.
Ovdje možete vidjet rezultat na jednoj od slika iz direktorija. Ovim postupkom smo postigli efekt šahovnice na jedan drugačiji način.

### Dimenzije 8x8
![CI8](./problem4/pepper_CI8.png "Pepper s invertiranim kvadratićima dimenzija 8x8")

### Dimenzije 16x16
![CI16](./problem4/pepper_CI16.png "Pepper s invertiranim kvadratićima dimenzija 16x16")

### Dimenzije 32x32
![CI32](./problem4/pepper_CI32.png "Pepper s invertiranim kvadratićima dimenzija 32x32")

# Problem 5
Zadatak je bio obraditi slike Floyd-Steinberg Dithering algoritmom, kako bi se poboljšala kvaliteta slika nakon što se one kvantiziraju.
Ovdje možete vidjet rezultat na jednoj od slika iz direktorija.
Na slikama kvantizacijske razine 1 se jako dobro mogu uočiti valovite strukture, koje su posljedica djelovanja algoritma.
Iz istog razloga, što se "razmuljaju" pikseli slika, možemo uočiti na rubovima da nisu oštri.
Možemo zaključiti - što je veća vrijednost kvantizacije, to je slika sličnija originalu - odnosno manja greška.

### Quantisation Value = 1
![1FSD](./problem5/lenna_1fsd.png "Lenna - Floyd-Steinberg Dithering - Q1")

### Quantisation Value = 2
![2FSD](./problem5/lenna_2fsd.png "Lenna - Floyd-Steinberg Dithering - Q2")

### Quantisation Value = 3
![3FSD](./problem5/lenna_3fsd.png "Lenna - Floyd-Steinberg Dithering - Q2")

# Problem 6
Zadatak je subsamplat sve slike unutar images direktorija, faktorima: 8, 16, 32.
Ovdje možete vidjet rezultat na jednoj od slika iz direktorija.
Ovim postupkom smo postigli zamutiti sliku, odnosno simulirati smanjenje slike iako je slika ostala istih dimenzija.

### Faktor 8
![F8](./problem6/baboon_sub8.png "Baboon subsamplan faktorom 8")

### Faktor 16
![F16](./problem6/baboon_sub16.png "Baboon subsamplan faktorom 16")

### Faktor 32
![F32](./problem6/baboon_sub32.png "Baboon subsamplan faktorom 2")

# Problem 7
Zadatak je prebaciti slike u YCRCB color space, odraditi sumbsampling iz Problema 6 na YCRCB slici, te vratiti kompresiranu sliku u BGR color space.
Nakon što se slika vrati u BGR color space, slika izgleda svjetlije.
Po meni je faktor subsamplinga = 4, maksimalni, sve ostalo je previše pikselizirano i pre svjetlo.
Za takvu kompresiju trebali bi 6 bit-a po pikselu.
Ovdje možete vidjet rezultat na jednoj od slika iz direktorija.

### Subsamping 2
![OG](./problem7/pepper_original.png "Pepper original")
![S2YCRCB](./problem7/pepper_ycrcb_2_subs.png "Pepper YCRCB Subsampled by factor 2")
![S2BGR](./problem7/pepper_bgr_2_subs.png "Pepper BGR Subsampled by factor 2")

### Subsamping 4
![OG](./problem7/pepper_original.png "Pepper original")
![S4YCRCB](./problem7/pepper_ycrcb_4_subs.png "Pepper YCRCB Subsampled by factor 4")
![S4BGR](./problem7/pepper_bgr_4_subs.png "Pepper BGR Subsampled by factor 4")

### Subsamping 8
![OG](./problem7/pepper_original.png "Pepper original")
![S8YCRCB](./problem7/pepper_ycrcb_8_subs.png "Pepper YCRCB Subsampled by factor 8")
![S8BGR](./problem7/pepper_bgr_8_subs.png "Pepper BGR Subsampled by factor 8")

### Subsamping 16
![OG](./problem7/pepper_original.png "Pepper original")
![S16YCRCB](./problem7/pepper_ycrcb_16_subs.png "Pepper YCRCB Subsampled by factor 16")
![S16BGR](./problem7/pepper_bgr_16_subs.png "Pepper BGR Subsampled by factor 16")
