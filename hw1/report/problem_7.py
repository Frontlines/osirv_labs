import cv2
import numpy as np
import os

PROJECT_PATH = "C:\\Users\\JURAJ.JURAJ-PC\\source\\repos\\osirv_labs"

def load_images(images_path, grayscale=1):
    """ Outputs: list of loaded images as numpy arrays and list of image filenames"""
    images = []
    image_names = []
    # os.listdir returns a list of filenames in a directory passed as parameter
    for image in os.listdir(images_path):
        image_path = os.path.join(images_path, image)
        if grayscale:
            open_as_flag = cv2.IMREAD_GRAYSCALE
        else:
            open_as_flag = cv2.IMREAD_COLOR
        print(image)
        images.append(cv2.imread(image_path, open_as_flag))
        image_names.append(image)
    return images, image_names

def show(image):
    # Break if trying to show image that is not uint8 datatype. You should always
    # convert your images to np.uint8 before trying to display them
    assert image.dtype == np.uint8, "Image is not np.uint8 datatype, convert to appropriate datatype"

    cv2.imshow("Image", image)
    cv2.waitKey()
    cv2.destroyAllWindows()

def save_image(image, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    print ("Base name for " + image_name + " is " + base_name)

    # Create absolute path to the project
    ######### Change this for your problem, e.g. os.path.join(PROJECT_PATH, "hw1", "problem_1")
    base_path = os.path.join(PROJECT_PATH, "hw1", "report", "problem7")
    save_path = os.path.join(base_path, base_name + append_string + ".png")
    print ("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

def subsample(array, factor):
    #array is a 2D numpy.array
    sub = array.astype(np.float32)

    horizontal_len = (sub.shape[0] // factor) + 1
    vertical_len = (sub.shape[1] // factor) + 1

    for vlength in range(0, vertical_len):
        for hlength in range(0, horizontal_len):
            segment = sub[hlength*factor:(hlength+1)*factor,vlength*factor:(vlength+1)*factor]
            if(segment.size > 0):
                average = int(round(segment.mean()))
                sub[hlength*factor:(hlength+1)*factor,vlength*factor:(vlength+1)*factor] = average

    return sub.astype(np.uint8)

images_path = os.path.join(PROJECT_PATH, "images")
images, image_names = load_images(images_path, grayscale=0)
img_dict = dict(zip(image_names, images))

for image in ["baboon.bmp","pepper.bmp"]:

    original = img_dict[image]
    show(original)
    save_image(original, image, "_original")

    ycrcb = cv2.cvtColor(original, cv2.COLOR_BGR2YCR_CB)
    
    for f in [2, 4, 8, 16]:
        subsampled = ycrcb.copy()
        for channel in range(0, subsampled.shape[2]):
            subsampled[:,:, channel] = subsample(subsampled[:,:, channel], f)

        bgr = cv2.cvtColor(subsampled, cv2.COLOR_YCR_CB2BGR)

        show(subsampled)
        show(bgr)
        save_image(subsampled, image, "_ycrcb_{0}_subs".format(f))
        save_image(bgr, image, "_bgr_{0}_subs".format(f))