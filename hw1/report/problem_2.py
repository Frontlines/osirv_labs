import cv2
import numpy as np
import os

PROJECT_PATH = "C:\\Users\\JURAJ.JURAJ-PC\\source\\repos\\osirv_labs"

def load_images(images_path, grayscale=1):
    """ Outputs: list of loaded images as numpy arrays and list of image filenames"""
    images = []
    image_names = []
    # os.listdir returns a list of filenames in a directory passed as parameter
    for image in os.listdir(images_path):
        image_path = os.path.join(images_path, image)
        if grayscale:
            open_as_flag = cv2.IMREAD_GRAYSCALE
        else:
            open_as_flag = cv2.IMREAD_COLOR
        print(image)
        images.append(cv2.imread(image_path, open_as_flag))
        image_names.append(image)
    return images, image_names

def show(image):
    # Break if trying to show image that is not uint8 datatype. You should always
    # convert your images to np.uint8 before trying to display them
    assert image.dtype == np.uint8, "Image is not np.uint8 datatype, convert to appropriate datatype"

    cv2.imshow("Image", image)
    cv2.waitKey()
    cv2.destroyAllWindows()

def save_image(image, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    print ("Base name for " + image_name + " is " + base_name)

    # Create absolute path to the project
    ######### Change this for your problem, e.g. os.path.join(PROJECT_PATH, "hw1", "problem_1")
    base_path = os.path.join(PROJECT_PATH, "hw1", "report", "problem2")
    save_path = os.path.join(base_path, base_name + append_string + ".png")
    print ("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

images_path = os.path.join(PROJECT_PATH, "images")
images, image_names = load_images(images_path, grayscale=0)
img_dict = dict(zip(image_names, images))

for image in list(img_dict):
    for thickness in [8, 16, 32]:
        vertical = img_dict[image].copy()
        horizontal = img_dict[image].copy()

        vertical_len = (vertical.shape[1] // thickness) + 1
        horizontal_len = (horizontal.shape[0] // thickness) + 1

        if(len(img_dict[image].shape) == 3):
            for length in range(0, vertical_len, 2):
                vertical[:,length*thickness:(length+1)*thickness,:] = 0

            for length in range(0, horizontal_len, 2):
                horizontal[length*thickness:(length+1)*thickness,:,:] = 0

        else:
            for length in range(0, vertical_len, 2):
                vertical[:,length*thickness:(length+1)*thickness] = 0

            for length in range(0, horizontal_len, 2):
                horizontal[length*thickness:(length+1)*thickness,:] = 0

        show(vertical)
        show(horizontal)
        save_image(vertical, image, "_V{0}".format(thickness))
        save_image(horizontal, image, "_H{0}".format(thickness))
    