import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy
import os

def save_image(image, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    print ("Base name for " + image_name + " is " + base_name)

    # Create absolute path to the project
    ######### Change this for your problem, e.g. os.path.join(PROJECT_PATH, "hw1", "problem_1")
    save_path = os.path.join("./problem4/", base_name + append_string + ".png")
    print ("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

img = cv2.imread('../slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)

for bs in [1, 3, 5]:

    dst = cv2.cornerHarris(gray,bs,3,0.04)

    #result is dilated for marking the corners, not important
    dst = cv2.dilate(dst,None)

    # threshold for an optimal value, it may vary depending on the image.
    img2[dst>0.01*dst.max()]=[0,0,255]

    plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    plt.title('Original Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(img2 , 'gray')
    plt.title('Detected Corners'), plt.xticks([]), plt.yticks([])
    plt.show()

    save_image(img,"chess.jpg","_original")
    save_image(cv2.cvtColor(img2,cv2.COLOR_RGB2BGR),"chess.jpg","_corners_blocksize{0}".format(bs))
