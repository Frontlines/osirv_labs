import cv2
import numpy as np
import math
import copy
from matplotlib import pyplot as plt
import os

def save_image(image, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    print ("Base name for " + image_name + " is " + base_name)

    # Create absolute path to the project
    ######### Change this for your problem, e.g. os.path.join(PROJECT_PATH, "hw1", "problem_1")
    save_path = os.path.join("./problem3/", base_name + append_string + ".png")
    print ("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

for thetav in [90, 180]:
    for thresholdv in [150, 200]:
        img = cv2.imread('../slike/chess.jpg')
        img2 = copy.copy(img)
        img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(gray, 166, 255)

        lines = cv2.HoughLines(edges, 1, math.pi/thetav, thresholdv, np.array([]), 0, 0)

        a,b,c = lines.shape
        for i in range(a):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0, y0 = a*rho, b*rho
            pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
            pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
            cv2.line(img2, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)

        plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        plt.title('Original Image'), plt.xticks([]), plt.yticks([])
        plt.subplot(122),plt.imshow(img2, 'gray')
        plt.title('Detected Lines'), plt.xticks([]), plt.yticks([])
        plt.show()

        save_image(img,"chess.jpg","_original")
        save_image(cv2.cvtColor(img2,cv2.COLOR_RGB2BGR),"chess.jpg","_lines_theta{0}_thresh{1}".format(thetav,thresholdv))

