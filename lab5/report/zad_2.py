import numpy as np
import argparse
import glob
import cv2
from matplotlib import pyplot as plt
import os

def save_image(image, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    print ("Base name for " + image_name + " is " + base_name)

    # Create absolute path to the project
    ######### Change this for your problem, e.g. os.path.join(PROJECT_PATH, "hw1", "problem_1")
    save_path = os.path.join("./problem2/", base_name + append_string + ".png")
    print ("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

def auto_canny(image, lowerlimit, upperlimit, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(lowerlimit, (1.0 - sigma) * v))
        upper = int(min(upperlimit, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print "Median lower: %r." % lower
        print "Median upper: %r." % upper
        # return the edged image
        return edged

# this function creates trackbar 
def nothing(x):
    pass

# infinite loop until we hit the escape key on keyboard
for image in ["airplane.bmp" , "barbara.bmp" , "boats.bmp" , "pepper.bmp"]:
    # read the image
    img = cv2.imread('../slike/{0}'.format(image), 0)
    # create trackbar for canny edge detection threshold changes
    cv2.namedWindow('canny')

    # add ON/OFF switch to "canny"
    switch = '0 : OFF \n1 : ON'
    cv2.createTrackbar(switch, 'canny', 0, 1, nothing)

    # add lower and upper threshold slidebars to "canny"
    cv2.createTrackbar('lower', 'canny', 0, 255, nothing)
    cv2.createTrackbar('upper', 'canny', 0, 255, nothing)

    while(1):

        # get current positions of four trackbars
        lower = cv2.getTrackbarPos('lower', 'canny')
        upper = cv2.getTrackbarPos('upper', 'canny')
        s = cv2.getTrackbarPos(switch, 'canny')

        if s == 0:
            edges = img
        else:
            edges = auto_canny(img,lower,upper)

    #   display images
        cv2.imshow('original', img)
        cv2.imshow('canny', edges)

        k = cv2.waitKey(1) & 0xFF
        if k == 27:   # hit escape to quit
            break
        elif k == 115:
            save_image(edges,image,"_l{0}_u{1}".format(lower,upper))

    cv2.destroyAllWindows()